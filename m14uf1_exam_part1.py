import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import os
from urllib.request import urlretrieve

"""
M14 UF1 Examen
Escriu el teu nom i cognoms.
"""
name:    str = 'Olga'
surname: str = 'Golovinskaya'
assert name,    'Please write your name'
assert surname, 'Please write your surname'

# Fitxer dataset original.
filepath: str = "incidence-2021-raw.csv"

# Et donem el contingut ja preprocessat, a partir de la pregunta 3.
filepath_q3: str = "incidence-2021-new.csv"

# Et donem una funció per llegir datasets. Pots editar-la si vols.
def q0_read_df_from_file(filepath):
    '''Retorna un dataframe des d'un fitxer CSV guardat localment.'''
    df_orig = pd.read_csv(filepath, sep=';', decimal=".")
    # print(df_orig.iloc[0])
    return df_orig

# Pregunta 1. Mostra la següent info: 
#   1.1 - Tota la informació de les columnes. Volem saber el total de files també.
#   1.2 - Les 5 primeres files
#   1.3 - La mitjana, màxim i desviació típica de la variable INCIDENCE_RATE
#   Pots mostrar més estadístiques opcionalment. 
def q1_basic_info(df: pd.DataFrame):
    #   1.1
    print(len(df))

    print(df.info())
    #   1.2 - Les 5 primeres files
    print(df.head())
#   1.3 - La mitjana, màxim i desviació típica de la variable INCIDENCE_RATE
#   Pots mostrar més estadístiques opcionalment.
    media = df["INCIDENCE_RATE"].mean()
    print(media)
    max = df["INCIDENCE_RATE"].max()
    print(max)
    desviacio =  df['INCIDENCE_RATE'].std()
    print(desviacio)

# Pregunta 2. Preprocessament dataframe dataframe.
# Crea un nou dataframe que:
#    2.1 - Elimini la columna DISEASE_DESCRIPTION
#    2.2 - De la columna 'DENOMINATOR' selecciona només 
#         les files que tinguin el valor
#         'per 1,000,000 total population'.
#         per tal d'unificar criteris.
#    2.3 - De la columna 'GROUP' selecciona només les files
#         que tinguin el valor 'COUNTRIES'
#    2.4 - Un cop fet, elimina la columna 'DENOMINATOR' i 'GROUP'
#    Guarda-ho en un fitxer amb el nom indicat a la variable 
#       filepath_q2.
def q2_preprocessing(df: pd.DataFrame):
    # Crea un nou dataframe que:
    df_new = df
    #    2.1 - Elimini la columna DISEASE_DESCRIPTION
    df_new =  df_new.drop(["DISEASE_DESCRIPTION"], axis=1)
    print(df_new.head())
#    2.2 - De la columna 'DENOMINATOR' selecciona només 
#         les files que tinguin el valor
#         'per 1,000,000 total population'.
#         per tal d'unificar criteris.
    
    df_new = df_new[df_new['DENOMINATOR'] == 'per 1,000,000 total population']
    print(df_new.head())
#    2.3 - De la columna 'GROUP' selecciona només les files
#         que tinguin el valor 'COUNTRIES'
    df_new= df_new[df_new['GROUP'] == 'COUNTRIES']
    print(df_new.head())
    #    2.4 - Un cop fet, elimina la columna 'DENOMINATOR' i 'GROUP'
#    Guarda-ho en un fitxer amb el nom indicat a la variable 
#       filepath_q2.
    df = df.drop(columns=['DENOMINATOR', 'GROUP'])
    filepath_q2= "q2_dataset.csv"
    df.to_csv(filepath_q2, index=False)




# Pregunta 3. Analitzem NaN i outliers.
#    3.1 - Mostra els valors NaN que hi ha en cada columna.
#    3.2 - Mostra els valors únics de les enfermetats (columna 'DISEASE')
#    3.3 - Mostra el número de casos de cada enfermetat
#    3.4 - Mostra el nom, enfermetat, inc_rate i any de les 10 files que tenen valors més
#        alts d'INCIDENCE_RATE.
def q3_outliers(df_new: pd.DataFrame):
    #    3.1 - Mostra els valors NaN que hi ha en cada columna.
    print("Mostra els valors NaN que hi ha en cada columna.", df_new.isna().sum())
    #    3.2 - Mostra els valors únics de les enfermetats (columna 'DISEASE')

    print("Mostra els valors únics de les enfermetats (columna 'DISEASE')", df_new['DISEASE'].unique())

    # 3.3 - Mostra el nombre de casos de cada enfermetat.
    print("Mostra el nombre de casos de cada enfermetat: ", df_new['DISEASE'].value_counts())


#    3.4 - Mostra el nom, enfermetat, inc_rate i any de les 10 files que tenen valors més
#        alts d'INCIDENCE_RATE.
    incidents = df_new.sort_values(by="INCIDENCE_RATE", ascending=False).head(10)
    columnas = incidents[["NAME", 
         "DISEASE",
         "YEAR",
         "INCIDENCE_RATE"]]
    print("el nom, enfermetat, inc_rate i any de les 10 files que tenen valors més alts d'INCIDENCE_RATE:", columnas)



# Pregunta 4. Afegim noves observacions al dataframe. 
#   Ens han arribat noves dades del 2022 de Measles i Pertussis.
#   Es troben al fitxers inc_measles_2022.csv i inc_pertussis_2022.csv
#   Has de concatenar aquests 2 fitxers al dataframe anterior,
#   Finalment has de crear un nou fitxer anomenat:
#      incidence-2022-new.csv
#   Pista: Assegura't que els noms de les columnes coincideixin.
#      i també el seu ordre.
def q4_concat_2022_rows(df_new: pd.DataFrame):
    print('TODO')
    pertussis_2022 = pd.read_csv('inc_pertussis_2022.csv',sep=';', decimal=".")
    pertussis_2022 = pertussis_2022.rename(columns={'2022': 'INCIDENCE_RATE'})
    pertussis_2022 = pertussis_2022.assign(Year=2022)
    pertussis_2022 = pertussis_2022.reindex(columns=["CODE","NAME","YEAR","DISEASE","INCIDENCE_RATE"])
    print(pertussis_2022.head())

    measles_2022 = pd.read_csv('inc_measles_2022.csv',sep=';', decimal=".")
    measles_2022 = measles_2022.rename(columns={'2022': 'INCIDENCE_RATE'})
    measles_2022 = measles_2022.assign(Year=2022)
    measles_2022 = measles_2022.reindex(columns=["CODE","NAME","YEAR","DISEASE","INCIDENCE_RATE"])
    print(measles_2022.head())
    df_new = df_new.reindex(columns=["CODE","NAME","YEAR","DISEASE","INCIDENCE_RATE", "DISEASE_DESCRIPTION",  "DENOMINATOR", "GROUP" ])
    print(df_new.head())
    df_2022 = pd.concat([df_new, measles_2022, pertussis_2022], ignore_index=True)
    print(df_2022.head(20))
    
    df_2022.to_csv('datos-2022-new.csv', index=False)




# Pregunta 5. Gràfic heatmap de la incidència del Tifus (Tiphoid)
#   Ens ha arribat un nou dataset que conté en diverses columnes la 
#   incidència dels anys 2018-2022 a cada país.
#       inc_typhoid_2018_2022.csv
#   Feu una taula de calor dels 5 anys en 5 països.
#   Pista:
#     - https://seaborn.pydata.org/generated/seaborn.heatmap.html
def q5_grafic_tiphoid(filename_q5: str):
    df = pd.read_csv(q5_filename,sep=';', decimal=".")
    print(df.head())
    countries = ["Angola", "Australia", "Bahrain", "Burkina Faso", "Ecuador"]
    years = ["2022", "2021", "2020", "2019", "2018"]
    df_para_heatmap = df[df['NAME'].isin(countries) ]
    
    df_para_heatmap.fillna(0)
    df_para_heatmap = df_para_heatmap.replace(['NaN'], 0)
    print(df_para_heatmap)

"""     plt.figure(figsize=(10, 6))
    heatmap = df_para_heatmap("Country", "Year", "Incidence")
    sns.heatmap(heatmap, annot=True, cmap="YlGnBu", fmt=".2f", linewidths=.5)
    plt.title("Tifus in  2018-2022 in Angola, Australia, Bahrain, Burkina Faso, Ecuador")
    plt.show() """ 




if __name__ == "__main__":
    df = q0_read_df_from_file(filepath)
    q1_basic_info(df)
    df_new = q2_preprocessing(df)
    df_new = pd.read_csv(filepath, sep=';', decimal=".")
    q3_outliers(df_new)
    q4_concat_2022_rows(df_new)
    # df_new = q4_concat_2022_rows(df_new)
    q5_filename = 'inc_typhoid_2018_2022.csv'
    q5_grafic_tiphoid(q5_filename)

