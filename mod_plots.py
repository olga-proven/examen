import pandas as pd
import matplotlib.pyplot as plt
import plotly
import plotly.express as px
import seaborn as sns
import base64
from io import BytesIO
from m14uf1_exam_part1 import *

def plot_to_base64(plt):
    '''Funció per convertir els gràfics en format base64 per a la visualització a la web'''
    img = BytesIO()
    plt.savefig(img, format='png')
    # plt.close()
    img.seek(0)
    return base64.b64encode(img.getvalue()).decode()

def __draw_plot():
    '''Transforma el gràfic en una imatge.'''
    img = BytesIO()
    plt.savefig(img, format='png')
    plt.close()
    img.seek(0)
    plot_url = base64.b64encode(img.getvalue()).decode('utf8')
    return plot_url


def demo_plot():
    '''Carrega un gràfic de prova del dataframe del Titanic. '''
    titanic_df = sns.load_dataset("titanic")
    print(titanic_df.head())
    # Another way to visualize the data is to use FacetGrid to plot multiple kedplots on one plot
    fig = sns.FacetGrid(titanic_df, hue="sex", aspect=4)
    fig.map(sns.kdeplot, 'age', fill=True)
    new_plot = titanic_df['age'].max()
    fig.set(xlim=(0, new_plot))
    fig.add_legend()
    plot_url = __draw_plot()
    return plot_url
    df = pd.read_csv('./output/incidence-2022-new-q4.csv',sep=';', decimal=".")
# Pregunta 8. Mostra 2 gràfics a la pàgina web
#     8.1 - Un on es vegi la incidència de les enfermetats d'un país(el que tu vulguis) 
#     des del 1980 fins el 1989, ambdós inclosos.

def q8_grafic1():
    df = pd.read_csv('./output/incidence-2022-new-q4.csv',sep=';', decimal=".")
    zimbabwe =  df[df['NAME'] == "Zimbabwe"]
    zimbabwe = zimbabwe[(zimbabwe['YEAR'] >= 1980) & (zimbabwe['YEAR'] <= 1989)]
    print(zimbabwe.head(10))
    zimbabwe_casos =  zimbabwe[[
         "DISEASE",
          "INCIDENCE_RATE"]]
    print(zimbabwe_casos.head(10))
    
   

    #sns.barplot(x='DISEASE', y='INCIDENCE_RATE', data=casos)
    plt.figure(figsize=(18, 8))  # the size of the figure
    sns.set(style="ticks")  # the style
    graph = sns.barplot(data=zimbabwe_casos, x='DISEASE', y='INCIDENCE_RATE')
    
    # Put labels and title
    graph.set(xlabel="Disease", ylabel="Number of cases")

    graph.set_xlabel("Disease", fontsize=12)  # Font size for x-label
    graph.set_ylabel("Number of cases", fontsize=12)  # Font size for y-label
    #graph.title("Diseases in Zimbabwe")
    plt.savefig('graph_1.png')
    plt.show()
    plt.close()


#     8.2 -  Un gràfic que serveixi per comparar les dades d'una enfermetat amb molta incidència (per exemple MEASLES) Ç
#     a una agrupació de països del món que tingui almenys 3 països.
#     Tens 2 gràfics de mostra a la carpeta output (q6_plot1 i q6_plot2) però pensa
#     que no són exactament iguals als que has d'aconseguir.


def map1(df):

    df_map = df.query("DISEASE" == 'PERTUSSIS')
    df_map = df_map.sort_values(by=['YEAR','INCIDENCE_RATE'], ascending = [True, False])
    

    # Crea el mapa de cloropets
    fig = px.choropleth(df_map,
                        locations='CODE',  # Nom del país
                        locationmode='ISO-3',  # Mode de localització per noms de país [ISO-3, country names]
                        hover_name='NAME',  # Informació que apareixerà en la caixa d'eines en fer hover
                        color='INCIDENCE_RATE',  # Variable a representar amb colors
                        color_continuous_scale=px.colors.sequential.Reds,
                        animation_frame='YEAR',
                        title='Mapa casos de Xarampió als països'
                    )


    # # Mostra el mapa
    fig.update_layout(margin=dict(l=20,r=0,b=0,t=70,pad=0),paper_bgcolor="white",height= 700,font_size=18)
    return fig


q8_grafic1()