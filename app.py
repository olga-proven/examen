from flask import Flask, render_template, request, Response
from flask_restful import Resource, Api
import pandas as pd
import matplotlib.pyplot as plt
import plotly
import plotly.express as px
import json
# Our modules.
import m14uf1_exam_part1
import mod_plots

app = Flask(__name__)
api = Api(app)

df = m14uf1_exam_part1.q0_read_df_from_file('./output/incidence-2022-new-q4.csv')
print(df)

# WEB SERVICES 

@app.route("/observation")
def patients():
    data = df.head(10)
    return Response(
        data.to_json(orient="records"),
        mimetype='application/json')


@app.route("/observation/<field>/<value>")
def query_field_value(field, value):
    data = df[df[field] == value]
    if data.empty:
        return {"error": "no data found", "field": field, "value": value}, 404

    # data = data.to_json(orient="records")
    return Response(
        data.to_json(orient="records"),
        mimetype='application/json')


@app.route("/observation/<field1>/<value1>/<field2>/<value2>")
def query_para_q7(field1, value1, field2, value2):
    data = df[(df[field1] == value1) & (df[field2] == value2)]
    
    if data.empty:
        return {"error": "no data found", "field1": field1, "value1": value1, "field2": field2, "value2": value2}, 404

    return Response(
        data.to_json(orient="records"),
        mimetype='application/json')


# WEB PAGE ROUTES
@app.route("/")
def index():
    return render_template("index.html",title="Pe1")

# Pregunta 6. Web en Flask i templates Jinja.
#   Has d'aconseguir integrar la pàgina q6_main.html
#   A la estructura de la web, modificant el base.html i l'app.py


# Pregunta 7. Crea 2 serveis web GET:
#      Un que mostri les 10 primeres files. Sintaxi:
#        ./observation
#      Un altre que filtri pel valor1 d'un field1 i 
#        un valor2 d'un field2.
#      Per exemple, que filtri els casos de Measles a Espanya 
#      La sintaxi de la petició GET serà:
#         ./observation/<field1>/<value1>/<field2>/<value2>
@app.route("/q7_api")
def q7_api():
    return render_template("q7_api.html",title="Pe1-APIs")


# Pregunta 8. Mostra 2 gràfics a la pàgina web
#     8.1 - Un on es vegi la incidència de les enfermetats d'un país(el que tu vulguis) 
#     des del 1980 fins el 1989, ambdós inclosos.
#     8.2 -  Un gràfic que serveixi per comparar les dades d'una enfermetat amb molta incidència (per exemple MEASLES) Ç
#     a una agrupació de països del món que tingui almenys 3 països.
#     Tens 2 gràfics de mostra a la carpeta output (q6_plot1 i q6_plot2) però pensa
#     que no són exactament iguals als que has d'aconseguir.
#     Pista: amb la funció de Pandas is_in pots seleccionar molts.


@app.route("/q8_plots")
def q8_plots():
    grafic1 = mod_plots.q8_grafic1()
    grafic2 = mod_plots.q8_grafic2()
    # Només és un exemple.
    south_mediterrean_countries: list[str] = ['Morocco', 'Algeria', 'Libya', 'Egypt', 'Jordan', 'Israel']
    return render_template("q8_plots.html",title="Pe1-Plots",grafic1=grafic1)

# Pregunta 9. Crea un mapa del món on es mostri la evolució dels casos de 
# PERTUSSIS cada any a tots els països amb Plotly.
@app.route("/q9_map")
def q9_map():
    fig = mod_plots.map1(df)
    graphJSON = json.dumps(fig, cls=plotly.utils.PlotlyJSONEncoder)
    return render_template('q9_map.html',title="Pe1-Map",graphJSON=graphJSON)

# MAIN

if __name__ == "__main__":
    #app.run()
    app.run(debug=True)